# ansible-kodi #

This role installs kodi and optionnally auto-start it when starting up a user's session.

usage: 


```
#!yaml

- hosts: my.host.example.com
  roles:
   - dledanseur.kodi
     vars:
       kodi_autostart_user: auser # optional, default to none
```